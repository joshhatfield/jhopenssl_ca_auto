#!/bin/bash

source $2

export ROOTDIR="${CADIR}/${CANAME}"

export INTERROOTDIR=${ROOTDIR}/intermediate


format_openssl_cnf () {
	sed -i "s#ROOTSEDDIR#$ROOTDIR#g" ${ROOTDIR}/openssl.cnf
	sed -i "s#INTSEDDIR#$INTERROOTDIR#g" ${INTERROOTDIR}/openssl.cnf
	sed -i "s#OCSPURL#$CAOCSPURI#g" ${ROOTDIR}/openssl.cnf
	sed -i "s#OCSPURL#$CAOCSPURI#g" ${INTERROOTDIR}/openssl.cnf

	
}

case $1 in
	format)
format_openssl_cnf
		;;
esac
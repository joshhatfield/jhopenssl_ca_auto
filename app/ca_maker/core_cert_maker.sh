#!/bin/bash

source $2

export ROOTDIR="${CADIR}/${CANAME}"

export INTERROOTDIR=${ROOTDIR}/intermediate


make_root_cert () {
	openssl req -config ${ROOTDIR}/openssl.cnf \
	  -passin file:"${ROOTDIR}/private/key.password" \
      -key ${ROOTDIR}/private/ca.key.pem \
      -new -x509 -days 18250 -sha256 -extensions v3_ca \
      -out ${ROOTDIR}/certs/ca.cert.pem \
      -subj /C=$"$COUNTRY"/ST="$STATE"/L="$LOCALITY"/O="$ORG"/OU="$ORGUNIT"/CN="$ROOTCOMMONNAME"/emailAddress="$EMAIL"
}

make_intermediate_csr () {
	openssl req -config ${INTERROOTDIR}/openssl.cnf -new -sha256 \
	-passin file:"${INTERROOTDIR}/private/key.password" \
      -key ${INTERROOTDIR}/private/intermediate.key.pem \
      -out ${INTERROOTDIR}/csr/intermediate.csr.pem \
      -subj /C=$"$COUNTRY"/ST="$STATE"/L="$LOCALITY"/O="$ORG"/OU="$ORGUNIT"/CN="$INTERMEDIATECOMMONNAME"/emailAddress="$EMAIL"
}

sign_intermediate_cert () {
	yes | openssl ca -config ${ROOTDIR}/openssl.cnf \
	  -passin file:"${ROOTDIR}/private/key.password" \
	  -extensions v3_intermediate_ca \
      -days 3650 -notext -md sha256 \
      -in ${INTERROOTDIR}/csr/intermediate.csr.pem \
      -out ${INTERROOTDIR}/certs/intermediate.cert.pem

}

make_ca_chain () {
	cat ${INTERROOTDIR}/certs/intermediate.cert.pem \
      ${ROOTDIR}/certs/ca.cert.pem > ${INTERROOTDIR}/certs/ca-chain.cert.pem
}

case $1 in
	certmake)
make_root_cert
make_intermediate_csr
sign_intermediate_cert
make_ca_chain
		;;
esac
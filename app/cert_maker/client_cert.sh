#!/bin/bash

source $2

export ROOTDIR="${CADIR}/${CANAME}"

export INTERROOTDIR=${ROOTDIR}/intermediate

export USRDIR="${ROOTDIR}/usr_cert"


make_cert_dir () {
	mkdir -p ${USRDIR}/${USRCERTNAME}
	export USRCERTDIR=${USRDIR}/${USRCERTNAME}
}

gen_rsa_key () {
	openssl rand -base64 8 > ${USRCERTDIR}/key.password
	export USRCERTPW=${USRCERTDIR}/key.password
}

gen_ssl_key () {
	openssl genrsa -aes256 -passout file:"${USRCERTPW}" -out ${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.key.pem 4096
	export USRKEY=${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.key.pem
}


make_usr_csr () {
	openssl req -config ${INTERROOTDIR}/openssl.cnf \
	  -passin file:"${USRCERTPW}" \
      -key ${USRKEY} \
      -new -sha256 -out ${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.csr.pem \
      -subj /C=$"$USRCOUNTRY"/ST="$USRSTATE"/L="$USRLOCALITY"/O="$USRORG"/OU="$USRORGUNIT"/CN="$USRCERTNAME"/emailAddress="$USREMAIL"

    export USRCSR="${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.csr.pem"
}

sign_usr_csr () {
	yes | openssl ca -config ${INTERROOTDIR}/openssl.cnf \
	  -passin file:"${INTERROOTDIR}/private/key.password" \
	  -extensions usr_cert \
      -days 375 -notext -md sha256 \
      -in ${USRCSR} \
      -out ${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.cert.pem

    export USRCERTPEM=${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.cert.pem
}

create_chain_cert () {
	openssl x509 -in ${INTERROOTDIR}/certs/ca-chain.cert.pem -out ${USRDIR}/${USRCERTNAME}/ca-chain.pem
	export USRCACHAIN=${USRDIR}/${USRCERTNAME}/ca-chain.pem
}

make_base64_cert () {
	openssl x509 -in "${USRCERTPEM}" -out "${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.cer"
}

make_pfx_file () {
	openssl pkcs12 -export \
    -in "${USRCERTPEM}" -inkey "${USRKEY}" \
    -out "${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.p12" \
    -name ${USRCERTNAME} -CAfile "${INTERROOTDIR}/certs/ca-chain.cert.pem" -caname "${CANAME}" -chain \
    -passin file:"${USRCERTPW}" -password pass:$(cat ${USRCERTPW})

    USRP12FILE="${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.p12"
}

make_jks_file () {
	keytool -importkeystore -srckeystore "${USRP12FILE}" \
    -srcstoretype pkcs12 -srcstorepass $(cat ${USRCERTPW}) -srcalias ${USRCERTNAME} \
    -destkeystore "${USRDIR}/${USRCERTNAME}/${USRCERTNAME}.jks" -deststoretype jks \
    -deststorepass "$(cat ${USRCERTPW})"
}

zip_certs () {
	cd ${USRDIR}/${USRCERTNAME}
	tar -czvf ${USRCERTNAME}.tar.gz ./*
}


case $1 in
	gencert)
make_cert_dir
gen_rsa_key
gen_ssl_key
make_usr_csr
sign_usr_csr
create_chain_cert
make_base64_cert
make_pfx_file
make_jks_file
zip_certs
		;;
esac
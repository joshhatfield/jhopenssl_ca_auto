#!/bin/bash

source $2

export ROOTDIR="${CADIR}/${CANAME}"

export INTERROOTDIR=${ROOTDIR}/intermediate

export SVRDIR="${ROOTDIR}/server_cert"


make_cert_dir () {
	mkdir -p ${SVRDIR}/${SVRCERTNAME}
	export SVRCERTDIR=${SVRDIR}/${SVRCERTNAME}
}

gen_rsa_key () {
	openssl rand -base64 8 > ${SVRCERTDIR}/key.password
	export SVRCERTPW=${SVRCERTDIR}/key.password
}

gen_ssl_key () {
	openssl genrsa -aes256 -passout file:"${SVRCERTPW}" -out ${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.key.pem 4096
	export SVRKEY=${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.key.pem
}


make_SVR_csr () {
	openssl req -config ${INTERROOTDIR}/openssl.cnf \
	  -passin file:"${SVRCERTPW}" \
      -key ${SVRKEY} \
      -new -sha256 -out ${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.csr.pem \
      -subj /C=$"$SVRCOUNTRY"/ST="$SVRSTATE"/L="$SVRLOCALITY"/O="$SVRORG"/OU="$SVRORGUNIT"/CN="$SVRCERTNAME"/emailAddress="$SVREMAIL"

    export SVRCSR="${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.csr.pem"
}

sign_SVR_csr () {
	yes | openssl ca -config ${INTERROOTDIR}/openssl.cnf \
	  -passin file:"${INTERROOTDIR}/private/key.password" \
	  -extensions server_cert \
      -days 375 -notext -md sha256 \
      -in ${SVRCSR} \
      -out ${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.cert.pem

    export SVRCERTPEM=${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.cert.pem
}

create_chain_cert () {
	openssl x509 -in ${INTERROOTDIR}/certs/ca-chain.cert.pem -out ${SVRDIR}/${SVRCERTNAME}/ca-chain.pem
	export SVRCACHAIN=${SVRDIR}/${SVRCERTNAME}/ca-chain.pem
}

make_base64_cert () {
	openssl x509 -in "${SVRCERTPEM}" -out "${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.cer"
}

make_pfx_file () {
	openssl pkcs12 -export \
    -in "${SVRCERTPEM}" -inkey "${SVRKEY}" \
    -out "${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.p12" \
    -name ${SVRCERTNAME} -CAfile "${INTERROOTDIR}/certs/ca-chain.cert.pem" -caname "${CANAME}" -chain \
    -passin file:"${SVRCERTPW}" -password pass:$(cat ${SVRCERTPW})

    SVRP12FILE="${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.p12"
}

make_jks_file () {
	keytool -importkeystore -srckeystore "${SVRP12FILE}" \
    -srcstoretype pkcs12 -srcstorepass $(cat ${SVRCERTPW}) -srcalias ${SVRCERTNAME} \
    -destkeystore "${SVRDIR}/${SVRCERTNAME}/${SVRCERTNAME}.jks" -deststoretype jks \
    -deststorepass "$(cat ${SVRCERTPW})"
}

zip_certs () {
	cd ${SVRDIR}/${SVRCERTNAME}
	tar -czvf ${SVRCERTNAME}.tar.gz ./*
}


case $1 in
	gencert)
make_cert_dir
gen_rsa_key
gen_ssl_key
make_SVR_csr
sign_SVR_csr
create_chain_cert
make_base64_cert
make_pfx_file
make_jks_file
zip_certs
		;;
esac
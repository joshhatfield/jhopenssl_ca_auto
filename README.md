# Auto Openssh CA and issuer

To run this you must have the following installed

* GNU Make
* openssl
* java keytool (found in JDK)
* GNU Sed ( on OSX brew install gnu-sed --with-default-names )


This will generate an example file for an openssl CA, intermediate and user cert

## Use

set certificate details in config.sh

Run all commands from the root repo directory

### To build the CA

```
make build
```

You can reissue as many client/server certs as you want as long as the common name in
config.sh is unique.

### To issue a user cert 

```
make user-cert
```

### To issue a server cert

```
make server-cert
```

### To clean up / delete CA

```
make destroy
```

The directory created will looks like so

```
jhopenssl
├── certs
│   └── ca.cert.pem
├── crl
├── index.txt
├── index.txt.attr
├── index.txt.old
├── intermediate
│   ├── certs
│   │   ├── ca-chain.cert.pem
│   │   └── intermediate.cert.pem
│   ├── crl
│   ├── csr
│   │   └── intermediate.csr.pem
│   ├── index.txt
│   ├── index.txt.attr
│   ├── index.txt.attr.old
│   ├── index.txt.old
│   ├── newcerts
│   │   ├── 1000.pem
│   │   └── 1001.pem
│   ├── openssl.cnf
│   ├── private
│   │   ├── intermediate.key.pem
│   │   └── key.password
│   ├── serial
│   └── serial.old
├── newcerts
│   └── 1000.pem
├── openssl.cnf
├── private
│   ├── ca.key.pem
│   └── key.password
├── serial
├── serial.old
├── server_cert
│   └── auth.server.local
│       ├── auth.server.local.cer
│       ├── auth.server.local.cert.pem
│       ├── auth.server.local.csr.pem
│       ├── auth.server.local.jks
│       ├── auth.server.local.key.pem
│       ├── auth.server.local.p12
│       ├── auth.server.local.tar.gz
│       ├── ca-chain.pem
│       └── key.password
└── usr_cert
    └── www.example.com
        ├── ca-chain.pem
        ├── key.password
        ├── www.example.com.cer
        ├── www.example.com.cert.pem
        ├── www.example.com.csr.pem
        ├── www.example.com.jks
        ├── www.example.com.key.pem
        ├── www.example.com.p12
        └── www.example.com.tar.gz
 ```

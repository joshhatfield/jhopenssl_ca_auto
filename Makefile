



build:
	@bash app/ca_maker/buildca.sh build ./config.sh
	@bash app/ca_maker/caformatter.sh format ./config.sh
	@bash app/ca_maker/core_cert_maker.sh certmake ./config.sh
	

user-cert:
	@bash app/cert_maker/client_cert.sh gencert ./config.sh

server-cert:
	@bash app/cert_maker/server_cert.sh gencert ./config.sh

destroy:
	@bash app/ca_maker/buildca.sh destroy ./config.sh

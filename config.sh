#!/bin/bash

#put config here

CADIR=`pwd`

#Root details

CANAME='jhopenssl'

CAOCSPURI='http://ocsp.example.com'

COUNTRY='AU'
STATE='NSW'
LOCALITY='root and intermediate'
ORG='JHatfield Certificate Authority'
ORGUNIT='Issued by Jhatfield'
EMAIL='josh@test.com'
ROOTCOMMONNAME='Jhatfield Root Certificate'
INTERMEDIATECOMMONNAME='Jhatfield Intermediate Certificate'



# Client Cert vars

USRCERTNAME='www.example.com'

USRCOUNTRY='AU'
USRSTATE='VIC'
USRLOCALITY='testcert'
USRORG='jjhcloud'
USRORGUNIT='web'
USREMAIL='josh@example.com'


# Server cert vars

SVRCERTNAME='auth.server.local'

SVRCOUNTRY='AU'
SVRSTATE='QLD'
SVRLOCALITY='servertestcert'
SVRORG='jjhcloud'
SVRORGUNIT='aws'
SVREMAIL='josh@admin.com'